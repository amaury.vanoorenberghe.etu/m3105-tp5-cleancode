package fr.univlille.iutinfo.m3105.viewQ1.slider;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SliderView extends Stage implements ITemperatureView, Observer {
	protected final Thermogeekostat model;
	private final SliderViewWidgets widgets;

	public SliderView(Thermogeekostat model) {
		this.model = model;
		model.temperatureProperty().attach(this);
		widgets = new SliderViewWidgets();
		widgets.setupWidgetsEventHandlers((e) -> model.incrementTemperature(), (e) -> model.decrementTemperature(),
				(e) -> model.setTemperature(widgets.getTemperature()));
		setScene(new Scene(widgets));
		setTitle("Slider");
		setResizable(false);
	}

	@Override
	public double getDisplayedValue() {
		return model.getTemperature();
	}

	@Override
	public void incrementAction() {
		model.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		model.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Subject subj, Object data) {
		if (model.temperatureProperty().equals(subj)) {
			Double value = (Double) data;
			widgets.slider.setValue(value);
		}
	}
}