package fr.univlille.iutinfo.m3105.viewQ1.slider;

import javafx.beans.InvalidationListener;
import javafx.event.*;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

public class SliderViewWidgets extends VBox {
	protected Slider slider;
	protected Button decrementBtn, incrementBtn;
	
	public SliderViewWidgets() {
		createWidgets();
	}

	public void createWidgets() {
		slider = new Slider(-100.0, 100.0, 0);
		decrementBtn = new Button("-");
		incrementBtn = new Button("+");		
		setAlignment(Pos.CENTER);
		setSpacing(4);
		getChildren().addAll(decrementBtn, slider, incrementBtn);
	}

	public void setupWidgetsEventHandlers(EventHandler<ActionEvent> incrementHandler, EventHandler<ActionEvent> decrementHandler, InvalidationListener sliderChangedHandler) {
		incrementBtn.setOnAction(incrementHandler);
		decrementBtn.setOnAction(decrementHandler);
		slider.valueProperty().addListener(sliderChangedHandler);
	}
	
	public double getTemperature() {
		return slider.getValue();
	}
}