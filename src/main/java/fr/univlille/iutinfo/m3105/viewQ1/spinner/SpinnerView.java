package fr.univlille.iutinfo.m3105.viewQ1.spinner;

import fr.univlille.iutinfo.m3105.modelQ1.Thermogeekostat;
import fr.univlille.iutinfo.m3105.utils.Observer;
import fr.univlille.iutinfo.m3105.utils.Subject;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class SpinnerView extends Stage implements ITemperatureView, Observer {
	private final Thermogeekostat MODEL;
	
	private Spinner<Double> spinner;
	private Button decrementBtn, incrementBtn;
	private HBox root;
	
	public SpinnerView(Thermogeekostat model) {
		MODEL = model;
		MODEL.temperatureProperty().attach(this);
		
		createWidgets();
		setupWidgetsEventHandlers();
		setupStage();
	}

	private void setupStage() {
		Scene scene = new Scene(root);
		setScene(scene);
		setTitle("Spinner");
		setResizable(false);
	}

	private void setupWidgetsEventHandlers() {
		decrementBtn.setOnAction((e) -> decrementAction());
		incrementBtn.setOnAction((e) -> incrementAction());
		spinner.valueProperty().addListener((e) -> MODEL.setTemperature(spinner.getValue()));
	}

	private void createWidgets() {
		spinner = new Spinner<Double>();
		DoubleSpinnerValueFactory spinFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(-100.0, 100.0, MODEL.getTemperature());
		spinner.setValueFactory(spinFactory);
		decrementBtn = new Button("-");
		incrementBtn = new Button("+");
		root = new HBox();
		root.setAlignment(Pos.CENTER);
		root.setSpacing(4);
		root.getChildren().addAll(decrementBtn, spinner, incrementBtn);
	}
	
	@Override
	public double getDisplayedValue() {
		return MODEL.getTemperature();
	}

	@Override
	public void incrementAction() {
		MODEL.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		MODEL.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Subject subj, Object data) {
		if (MODEL.temperatureProperty().equals(subj)) {
			Double value = (Double)data;
			spinner.getValueFactory().setValue(value);
		}
	}
}
