package fr.univlille.iutinfo.m3105.viewQ3.spinner;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.SpinnerValueFactory.DoubleSpinnerValueFactory;
import javafx.scene.layout.HBox;

public class SpinnerView extends SpinnerViewBase {
	public SpinnerView(Temperature model) {
		super(model);
		MODEL.temperatureProperty().attach(this);
		createWidgets();
		setupWidgetEventHandlers();
		setupStage();
		update(MODEL, MODEL.getTemperature());
	}

	private void createWidgets() {
		spinner = new Spinner<Double>();
		DoubleSpinnerValueFactory spinFactory = new SpinnerValueFactory.DoubleSpinnerValueFactory(-10000.0, 10000.0, MODEL.getTemperature());
		spinner.setValueFactory(spinFactory);
		decrementBtn = new Button("-");
		incrementBtn = new Button("+");
		root = new HBox();
		root.setAlignment(Pos.CENTER);
		root.setSpacing(4);
		root.getChildren().addAll(decrementBtn, spinner, incrementBtn);
	}

	private void setupWidgetEventHandlers() {
		decrementBtn.setOnAction((e) -> decrementAction());
		incrementBtn.setOnAction((e) -> incrementAction());
		spinner.valueProperty().addListener((e) -> MODEL.setTemperature(spinner.getValue()));
	}

	private void setupStage() {
		Scene scene = new Scene(root, 320, 240);
		setScene(scene);
		setResizable(false);
	}
}