package fr.univlille.iutinfo.m3105.viewQ3.slider;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

public class SliderView extends SliderViewBase {
	protected Button decrementBtn, incrementBtn;
	protected VBox root;
	
	public SliderView(Temperature model) {
		super(model);
		MODEL.temperatureProperty().attach(this);
		createWidgets();
		setupWidgetsEventHandler();
		setupStage();
		update(MODEL, MODEL.getTemperature());
	}

	private void setupStage() {
		Scene scene = new Scene(root, 320, 240);
		setScene(scene);
		setResizable(false);
	}

	private void createWidgets() {
		slider = new Slider(-10000.0, 10000.0, MODEL.getTemperature());	
		decrementBtn = new Button("-");
		incrementBtn = new Button("+");
		root = new VBox();
		root.setAlignment(Pos.CENTER);
		root.setSpacing(4);
		root.getChildren().addAll(decrementBtn, slider, incrementBtn);
	}

	private void setupWidgetsEventHandler() {
		decrementBtn.setOnAction((e) -> decrementAction());
		incrementBtn.setOnAction((e) -> incrementAction());
		slider.valueProperty().addListener((e) -> MODEL.setTemperature(slider.getValue()));
	}
}