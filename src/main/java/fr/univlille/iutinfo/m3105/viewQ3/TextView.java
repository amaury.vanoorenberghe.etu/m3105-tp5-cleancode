package fr.univlille.iutinfo.m3105.viewQ3;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.spinner.SpinnerView;

/**
 * Classe présente uniquement dans le but de ne pas casser les tests
 * La vue est implémentée dans SpinnerView.java
 */
public class TextView extends SpinnerView {
	public TextView(Temperature model) {
		super(model);
	}
}
