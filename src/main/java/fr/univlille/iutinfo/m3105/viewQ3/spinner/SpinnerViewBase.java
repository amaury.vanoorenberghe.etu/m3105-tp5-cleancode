package fr.univlille.iutinfo.m3105.viewQ3.spinner;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.*;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public abstract class SpinnerViewBase extends Stage implements ITemperatureView, Observer {
	protected final Temperature MODEL;
	
	protected Spinner<Double> spinner;
	protected Button decrementBtn, incrementBtn;
	protected HBox root;
	
	protected SpinnerViewBase(Temperature model) {
		MODEL = model;
	}
	
	@Override
	public double getDisplayedValue() {
		return MODEL.getTemperature();
	}

	@Override
	public void incrementAction() {
		MODEL.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		MODEL.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Subject subj, Object data) {
		if (MODEL.temperatureProperty().equals(subj)) {
			Double value = MODEL.getTemperature();
			spinner.getValueFactory().setValue(value);
			setTitle(MODEL.getEchelle().getName());
		}
	}
}
