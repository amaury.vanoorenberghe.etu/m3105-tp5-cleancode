package fr.univlille.iutinfo.m3105.viewQ3.slider;

import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.utils.*;
import fr.univlille.iutinfo.m3105.viewQ1.ITemperatureView;
import javafx.scene.control.Slider;
import javafx.stage.Stage;

public abstract class SliderViewBase extends Stage implements ITemperatureView, Observer {
	protected final Temperature MODEL;
	protected Slider slider;
	
	protected SliderViewBase(Temperature model) {
		MODEL = model;
	}
	
	@Override
	public double getDisplayedValue() {
		return MODEL.getTemperature();
	}

	@Override
	public void incrementAction() {
		MODEL.incrementTemperature();
	}

	@Override
	public void decrementAction() {
		MODEL.decrementTemperature();
	}

	@Override
	public void update(Subject subj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Subject subj, Object data) {
		if (MODEL.temperatureProperty().equals(subj)) {
			Double value = MODEL.getTemperature();
			slider.setValue(value);
			setTitle(MODEL.getEchelle().getName());
		}
	}
}