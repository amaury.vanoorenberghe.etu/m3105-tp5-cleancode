package fr.univlille.iutinfo.m3105;

import fr.univlille.iutinfo.m3105.modelQ2.Echelle;
import fr.univlille.iutinfo.m3105.modelQ2.Temperature;
import fr.univlille.iutinfo.m3105.viewQ3.slider.SliderView;
import fr.univlille.iutinfo.m3105.viewQ3.spinner.SpinnerView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// === QUESTION 1 ===
		/*
		Thermogeekostat thermo = new Thermogeekostat();
		new SpinnerView(thermo).show();
		new SliderView(thermo).show();
		*/
		
		// === QUESTION 3 ===
		Temperature tempKelvin = new Temperature(Echelle.KELVIN);
		Temperature tempCelsius = new Temperature(Echelle.CELSIUS);
		Temperature tempNewton = new Temperature(Echelle.NEWTON);
		
		tempKelvin.biconnectTo(tempCelsius);
		tempCelsius.biconnectTo(tempNewton);
		
		new SpinnerView(tempKelvin).show();
		new SpinnerView(tempCelsius).show();
		new SliderView(tempNewton).show();
		
	}
}
