package fr.univlille.iutinfo.m3105.modelQ2.conversions;

public class Rankine {
	public static Double RANKINE_FACTOR = Fahrenheit.FAHRENHEIT_FACTOR;

	public static Double kelvinToRankine(Double kelvin) {
		return RANKINE_FACTOR * kelvin;
	}

	public static Double rankineToKelvin(Double rankine) {
		return rankine / RANKINE_FACTOR;
	}
}