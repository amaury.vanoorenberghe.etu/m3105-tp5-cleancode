package fr.univlille.iutinfo.m3105.modelQ2.conversions;

public class Reaumur {
	public static final Double REAUMUR_OFFSET = Celsius.CELSIUS_OFFSET;
	public static final Double REAUMUR_FACTOR = 4.0 / 5.0;

	public static Double kelvinToReaumur(Double kelvin) {
		return (kelvin * REAUMUR_FACTOR) - REAUMUR_OFFSET;
	}

	public static Double reaumurToKelvin(Double reaumur) {
		return (reaumur + REAUMUR_OFFSET) / REAUMUR_FACTOR;
	}
}
