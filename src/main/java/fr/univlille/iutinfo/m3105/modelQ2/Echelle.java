package fr.univlille.iutinfo.m3105.modelQ2;

import java.util.function.Function;
import fr.univlille.iutinfo.m3105.modelQ2.conversions.*;;

public enum Echelle {
	//DELISLE	("Delisle", 	"°De"),
	//LEYDEN	("Leyden", 		"°Le"),
	//ROMER		("Røner", 		"°Rø"),
	KELVIN		("Kelvin", 		"K", 	Kelvin::kelvinToKelvin, 		Kelvin::kelvinToKelvin),
	CELSIUS		("Celsius", 	"C", 	Celsius::kelvinToCelsius, 		Celsius::celsiusToKelvin), 
	NEWTON		("Newton", 		"N",	Newton::kelvinToNewton, 		Newton::newtonToKelvin),
	FAHRENHEIT	("Fahrenheit", 	"F",	Fahrenheit::kelvinToFahrenheit, Fahrenheit::fahrenheitToKelvin),
	RANKINE		("Rankine", 	"Ra", 	Rankine::kelvinToRankine,		Rankine::rankineToKelvin),
	REAUMUR		("Réaumur", 	"Ré",	Reaumur::kelvinToReaumur,		Reaumur::reaumurToKelvin);

	private final String NAME, ABBREVIATION;
	private final Function<Double, Double> FROM_KELVIN, TO_KELVIN;
	
	private Echelle(String name, String abbreviation, Function<Double, Double> fromKelvin, Function<Double, Double> toKelvin) {
		NAME = name;
		ABBREVIATION = abbreviation;
		FROM_KELVIN = fromKelvin;
		TO_KELVIN = toKelvin;
	}
	
	public String getName() {
		return NAME;
	}

	public String getAbbrev() {
		return ABBREVIATION;
	}

	public double fromKelvin(double d) {
		return FROM_KELVIN.apply(d);
	}

	public double toKelvin(double d) {
		return TO_KELVIN.apply(d);
	}
}
