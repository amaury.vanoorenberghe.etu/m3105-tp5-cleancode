package fr.univlille.iutinfo.m3105.modelQ2.conversions;

public class Fahrenheit {
	public static final Double FAHRENHEIT_OFFSET = 459.67;
	public static final Double FAHRENHEIT_FACTOR = 9.0 / 5.0;

	public static Double kelvinToFahrenheit(Double kelvin) {
		return (kelvin * FAHRENHEIT_FACTOR) - FAHRENHEIT_OFFSET;
	}

	public static Double fahrenheitToKelvin(Double fahrenheit) {
		return (fahrenheit + FAHRENHEIT_OFFSET) / FAHRENHEIT_FACTOR;
	}
}
