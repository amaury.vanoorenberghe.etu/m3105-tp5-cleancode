package fr.univlille.iutinfo.m3105.modelQ2.conversions;

public class Celsius {
	public static final Double CELSIUS_OFFSET = 273.15;

	public static Double kelvinToCelsius(Double kelvin) {
		return kelvin - CELSIUS_OFFSET;
	}

	public static Double celsiusToKelvin(Double celsius) {
		return celsius + CELSIUS_OFFSET;
	}
}
