package fr.univlille.iutinfo.m3105.modelQ2.conversions;

public class Newton {
	public static final Double NEWTON_OFFSET = Celsius.CELSIUS_OFFSET;
	public static final Double NEWTON_FACTOR = 100.0 / 33.0;

	public static Double kelvinToNewton(Double kelvin) {
		return (kelvin - NEWTON_OFFSET) / NEWTON_FACTOR;
	}

	public static Double newtonToKelvin(Double newton) {
		return (newton * NEWTON_FACTOR) + NEWTON_OFFSET;
	}
}
